#!/bin/bash

container_id=$((BUILD_NUMBER-1))
container_name="jobvacancy-$container_id"

should_stop=`sudo docker ps | grep "$container_name"`

set -e
if [ -n "$should_stop" ]; then
  echo "stop"
  #sudo docker stop $container_name
else
  echo "nothing to do"
fi
