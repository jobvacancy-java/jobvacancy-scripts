#!/bin/bash

old_port=`cat port.txt`

if [ "$old_port" == "9000" ]; then
  new_port=9001
else
  new_port=9000
fi
echo "$new_port" > port.txt
echo "Port: $new_port"

sudo docker run --name jobvacancy-${BUILD_NUMBER} -p $new_port:8080 -d ${IMAGE_NAME}