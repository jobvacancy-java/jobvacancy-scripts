#!/bin/bash

set -e
nginx_config="/etc/nginx/sites-available/default"
current=`cat $nginx_config | grep localhost;`

set +e
is_blue=`echo $current | grep 9000`
set -e
if [ -n "$is_blue" ]; then
  echo "running blue, now switching green"
  sed -i -e 's/localhost:9000/localhost:9001/g' $nginx_config
else
  echo "running green, now switching to blue"
  sed -i -e 's/localhost:9001/localhost:9000/g' $nginx_config
fi
sudo service nginx restart
