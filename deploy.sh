#!/bin/bash ﻿

set -e
set -x

echo "Verificando estado del servicio"
status=`sudo service jobvacancy status`
set +e
is_running=`echo $status | grep running`
set -e
if [ -n "$is_running" ]; then
  echo "running:$is_running."
  echo "Deteniendo el servicio"
  sudo service jobvacancy stop
fi

cp /home/jenkins/jobvacancy.war /opt/jobvacancy/jobvacancy.war
cp /home/jenkins/application-prod.yml /opt/jobvacancy/application-prod.yml
sudo service jobvacancy start