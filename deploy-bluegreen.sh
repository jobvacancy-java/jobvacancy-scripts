#!/bin/bash

set -x
touch /opt/jobvacancy/current
touch /opt/jobvacancy/pid
nginx_config="/etc/nginx/sites-available/default"
set +e
is_blue=`cat /opt/jobvacancy/current | grep blue`
set -e
if [ -n "$is_blue" ]; then
  echo "running blue, now switching green"
  active="green"
  port="9001"
  sudo sed -i -e 's/localhost:9000/localhost:9001/g' $nginx_config
else
  echo "running green, now switching to blue"
  active="blue"
  port="9000"
  sudo sed -i -e 's/localhost:9001/localhost:9000/g' $nginx_config
fi
sudo service nginx reload
echo "$active" > /opt/jobvacancy/current
cp /home/jenkins/jobvacancy.war /opt/jobvacancy/$active/jobvacancy.war
cp /home/jenkins/application-prod.yml /opt/jobvacancy/$active/application-prod.yml
cd /opt/jobvacancy/$active
BUILD_ID=dontKillMe nohup java -Djava.security.egd=file:/dev/./urandom -Dserver.port=$port -jar jobvacancy.war --spring.profiles.active=prod &
set +e
old_pid=`cat /opt/jobvacancy/pid`
echo $! > /opt/jobvacancy/pid
if [ -n "$old_pid" ]; then
  sudo kill -9 $old_pid
fi
