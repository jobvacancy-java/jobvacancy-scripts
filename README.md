To create the image run:

```
sudo docker build -t jobvacancy${BUILD_NUMBER} .
```

It expects the jobvacancy.war file to be in the current directory
