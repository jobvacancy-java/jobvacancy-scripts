SERVER_IP=$TARGET_SERVER
ssh-keyscan -H $SERVER_IP >> ~/.ssh/known_hosts
rm -f *.war
cp /var/lib/jenkins/workspace/Release/target/*.war .
war_name=`ls -t *.war | head -1`
mv $war_name jobvacancy.war

scp jobvacancy.war jenkins@${SERVER_IP}:/home/jenkins/jobvacancy.war
scp config/application-prod.yml jenkins@${SERVER_IP}:/home/jenkins/application-prod.yml
scp deploy.sh jenkins@${SERVER_IP}:/home/jenkins/deploy.sh


ssh jenkins@${SERVER_IP} 'bash /home/jenkins/deploy.sh'
