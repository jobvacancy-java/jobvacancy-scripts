FROM java:8
COPY . /usr/apps/jobvacancy
WORKDIR /usr/apps/jobvacancy
CMD ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "jobvacancy.war", "—spring.active.profile=prod"]
